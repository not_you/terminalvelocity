/* Copyright (C) 2019, 2020  Keyboard.io, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "config_common.h"


/* USB Device descriptor parameter */

#define VENDOR_ID       0x1209
#define PRODUCT_ID      0x2303
#define DEVICE_VER      0x0000
#define MANUFACTURER    Eris
#define PRODUCT         TerminalVelocity


#define USB_POLLING_INTERVAL_MS 1

/* key matrix size */
#define MATRIX_ROWS 4
#define MATRIX_COLS 12

/* define if matrix has ghost */
//#define MATRIX_HAS_GHOST

/* number of backlight levels */
//#define BACKLIGHT_LEVELS 3

/* Set 0 if debouncing isn't needed */
#define DEBOUNCE 5

/* Mechanical locking support. Use KC_LCAP, KC_LNUM or KC_LSCR instead in keymap */
#define LOCKING_SUPPORT_ENABLE
/* Locking resynchronize hack */
#define LOCKING_RESYNC_ENABLE

#define UNICODE_SELECTED_MODES UC_LNX

/*
 * Keyboard Matrix Assignments
 *
 * Change this to how you wired your keyboard
 * COLS: AVR pins used for columns, left to right
 * ROWS: AVR pins used for rows, top to bottom
 * DIODE_DIRECTION: COL2ROW = COL = Anode (+), ROW = Cathode (-, marked on diode)
 *                  ROW2COL = ROW = Anode (+), COL = Cathode (-, marked on diode)
 *
 */

#define MATRIX_ROW_PINS { GP0, GP1, GP2, GP3}
#define MATRIX_COL_PINS { GP4, GP5, GP6, GP7, GP8, GP9, GP10, GP11, GP12, GP13, GP14, GP15}

/* COL2ROW, ROW2COL*/
#define DIODE_DIRECTION COL2ROW

#define TAP_CODE_DELAY 10


//#define NOP_FUDGE 0.4
#define RGBLED_NUM 15 
#define RGB_DI_PIN GP16
//#define RGBLIGHT_ANIMATIONS
#define RGBLIGHT_HUE_STEP 8
#define RGBLIGHT_SAT_STEP 50

#define RGBLIGHT_EFFECT_RAINBOW_SWIRL

#define WS2812_TIMING 1250
#define WS2812_T0H 350
#define WS2812_T0L WS2812_TIMING - WS2812_T0H
#define WS2812_T1H 900
#define WS2812_T1L WS2812_TIMING - WS2812_T1H

/* disable debug print */
//#define NO_DEBUG

/* disable print */
//#define NO_PRINT

/* disable action features */
//#define NO_ACTION_LAYER
//#define NO_ACTION_TAPPING
//#define NO_ACTION_ONESHOT
//#define NO_ACTION_MACRO
//#define NO_ACTION_FUNCTION

/*TrackPoint Conf*/
#define SPI_DRIVER SPID1
#define SPI_SCK_PIN GP26
#define SPI_MOSI_PIN GP27
#define SPI_MISO_PIN GP28

#define TRACKPOINT_CS_PIN GP29
#define TRACKPOINT_SPI_MODE 3
#define TRACKPOINT_SPI_DIVISOR 133
#define MOUSE_EXTENDED_REPORT
//#define POINTING_DEVICE_TASK_THROTTLE_MS 1

