# MCU name
MCU = RP2040

BOOTLOADER = rp2040

# Build Options
#   change yes to no to disable
#
BOOTMAGIC_ENABLE = yes # Virtual DIP switch configuration

#DO NOT ENABLE MOUSEKEYS: MOYSEKEYCODES GET OVERRIDDEN BY POINTING DEVICE DRIVER
MOUSEKEY_ENABLE = no

EXTRAKEY_ENABLE = yes       # Audio control and System control
CONSOLE_ENABLE = no		# Console for debug
COMMAND_ENABLE = no 		# Commands for debug and configuration
# Do not enable SLEEP_LED_ENABLE. it uses the same timer as BACKLIGHT_ENABLE
SLEEP_LED_ENABLE = no       # Breathing sleep LED during USB suspend
# if this doesn't work, see here: https://github.com/tmk/tmk_keyboard/wiki/FAQ#nkro-doesnt-work
NKRO_ENABLE = yes           # USB Nkey Rollover
BACKLIGHT_ENABLE = no       # Enable keyboard backlight functionality
RGBLIGHT_ENABLE = yes # Enable keyboard RGB underglow
RGBLIGHT_DRIVER = WS2812
BLUETOOTH_ENABLE = no       # Enable Bluetooth
AUDIO_ENABLE = no           # Audio output

UNICODE_ENABLE = yes # Unicode
WS2812_DRIVER = vendor


NO_USB_STARTUP_CHECK = yes
POINTING_DEVICE_ENABLE = yes
POINTING_DEVICE_DRIVER = custom
SRC += trackpoint_driver.c
QUANTUM_LIB_SRC += spi_master.c
CONSOLE_ENABLE=yes
