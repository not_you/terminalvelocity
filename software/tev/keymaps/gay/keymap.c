#include QMK_KEYBOARD_H
#ifdef RGBLIGHT_ENABLE
#include "rgblight.h"
#endif
#include "keyboard.h"

const uint8_t RGBLED_RAINBOW_MOOD_INTERVALS[] PROGMEM = {30, 15, 7};
const uint8_t RGBLED_RAINBOW_SWIRL_INTERVALS[] PROGMEM = {25, 13, 5};


#define _LAYER0 0
#define _LAYER1 1
#define _LAYER2 2
#define _LAYER3 3
#define _LAYER5 5
#define _LAYER6 6
#define _LAYER7 7

enum custom_keycodes {
    LAYER0 = SAFE_RANGE,
    LAYER1,
    LAYER2,
    LAYER3,
    LAYER5,
    LAYER6,
    LAYER7,
};

 const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

 [_LAYER0] = LAYOUT(KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_BTN1, KC_BTN2, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_ESC, KC_TAB, KC_LALT, KC_LGUI, KC_SPC, TT(2), TT(1), LCTL_T(KC_BSPC), KC_LSFT, KC_MINS, KC_QUOT, KC_ENT),

[_LAYER1] = LAYOUT(KC_PSLS, KC_7, KC_8, KC_9, KC_PAST, KC_LCBR, KC_LBRC, KC_RBRC, KC_RCBR, KC_AT, KC_PMNS, KC_4, KC_5, KC_6, KC_PLUS, KC_LEFT, KC_DOWN, KC_UP, KC_RGHT, KC_PIPE, KC_DOT, KC_1, KC_2, KC_3, KC_EQL, KC_BTN1, KC_BTN2, KC_LPRN, KC_RPRN, KC_TILD, KC_AMPR, KC_HASH, KC_ESC, KC_TAB, LALT_T(KC_0), KC_LGUI, KC_DEL, KC_BTN3, KC_TRNS, LCTL_T(KC_BSPC), LSFT_T(KC_BSLS), KC_PERC, KC_EXLM, KC_ENT),

[_LAYER2] = LAYOUT(KC_PSCR, KC_F7, KC_F8, KC_F9, KC_F10, TO(5), KC_MPRV, KC_MPLY, KC_MNXT, TG(3), KC_BRIU, KC_F4, KC_F5, KC_F6, KC_F11, KC_VOLU, KC_VOLD, RGB_VAI, RGB_VAD, KC__MUTE, KC_BRID, KC_F1, KC_F2, KC_F3, KC_F12, KC_BTN1, KC_BTN2, KC_PGUP, KC_END, KC_HOME, KC_PGDN, KC_WH_D, KC_ESC, KC_TAB, KC_LALT, KC_LGUI, KC_SPC, TT(2), KC_BTN3, KC_LCTL, KC_LSFT, KC_BRIU, KC_BRID, KC_ENT),

[_LAYER3] = LAYOUT(KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_NO, KC_NO, KC_NO, KC_NO, TO(0), KC_ESC, KC_A, KC_S, KC_D, KC_G, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_LCTL, KC_Z, KC_X, KC_C, KC_F, KC_BTN1, KC_BTN2, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_LSFT, KC_I, KC_V, KC_M, KC_SPC, KC_SPC, KC_NO, KC_ENT, KC_NO, KC_NO, KC_NO, KC_NO),

[_LAYER5] = LAYOUT(KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_NO, MO(6), KC_N, KC_M, KC_DOT, KC_DOT, KC_SLSH, KC_NO, KC_NO, KC_NO, MO(7), KC_SPC, KC_NO, KC_NO, KC_NO, KC_LSFT, KC_MINS, KC_QUOT, KC_ENT),

[_LAYER6] = LAYOUT(KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_H, KC_J, KC_K, KC_L, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_NO, KC_TRNS, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_NO, KC_NO, KC_NO, KC_TRNS, KC_SPC, TO(0), KC_NO, KC_NO, KC_LSFT, KC_NO, KC_MINS, KC_QUOT),

[_LAYER7] = LAYOUT(KC_PSLS, KC_7, KC_8, KC_9, KC_PAST, KC_LCBR, KC_LBRC, KC_RBRC, KC_RCBR, KC_TILD, KC_PMNS, KC_4, KC_5, KC_6, KC_PPLS, KC_NO, KC_NO, KC_NO, KC_NO, KC_HASH, KC_AT, KC_1, KC_2, KC_3, KC_PEQL, KC_NO, KC_NO, KC_LPRN, KC_RPRN, KC_PIPE, KC_AMPR, KC_BSLS, KC_NO, KC_NO, KC_NO, KC_TRNS, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_PERC, KC_EXLM, KC_ENT)
 };

void keyboard_post_init_user(void) {
//	debug_enable=true;
	rgblight_enable();
    int value=rgblight_get_val();
	rgblight_set_effect_range(0, 3);
	rgblight_sethsv_noeeprom(140,210,value);
	rgblight_set_effect_range(3, 3);
	rgblight_sethsv_noeeprom(0,210,value);
	rgblight_set_effect_range(6, 3);
	rgblight_sethsv_noeeprom(0,0,value);
	rgblight_set_effect_range(9, 3);
	rgblight_sethsv_noeeprom(0,210,value);
	rgblight_set_effect_range(12, 3);
	rgblight_sethsv_noeeprom(140,210,value);
}

void matrix_scan_user(void) {
  #ifdef RGBLIGHT_ENABLE

  static uint8_t old_layer = 255;
  uint8_t new_layer = biton32(layer_state);

  int value=rgblight_get_val();
  if (old_layer != new_layer) {
    rgblight_mode(RGBLIGHT_MODE_STATIC_LIGHT);
    switch (new_layer) {
      case _LAYER0:
		rgblight_set_effect_range(0, 3);
		rgblight_sethsv_noeeprom(140,210,value);
		rgblight_set_effect_range(3, 3);
		rgblight_sethsv_noeeprom(0,210,value);
		rgblight_set_effect_range(6, 3);
		rgblight_sethsv_noeeprom(0,0,value);
		rgblight_set_effect_range(9, 3);
		rgblight_sethsv_noeeprom(0,210,value);
		rgblight_set_effect_range(12, 3);
		rgblight_sethsv_noeeprom(140,210,value);
	    break;
	case _LAYER1:
		rgblight_set_effect_range(0, 5);
		rgblight_sethsv_noeeprom(234,221,value);
		rgblight_set_effect_range(5, 5);
		rgblight_sethsv_noeeprom(36,255,value);
		rgblight_set_effect_range(10, 5);
		rgblight_sethsv_noeeprom(142,221,value);
        break;
      case _LAYER2:
		rgblight_set_effect_range(0, 15);
        rgblight_mode_noeeprom(RGBLIGHT_MODE_RAINBOW_SWIRL);
        break;
      case _LAYER3:
		rgblight_set_effect_range(0, 15);
		rgblight_sethsv_noeeprom(0,230,value);
        break;
     case _LAYER5:
		rgblight_set_effect_range(0, 15);
		rgblight_sethsv_noeeprom(50,250,value);
        break;

    }

    old_layer = new_layer;
  }


  #endif //RGBLIGHT_ENABLE
}

/*bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  return true;
}*/
