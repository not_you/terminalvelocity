#include "mcp3202.c"
#include "host.h"
#include "math.h"
#include <quantum.h>
#include "print.h"
#include "keyboard.h"
#include "trackpoint_driver.h"

#define DEADZONE 10
#define ZONECAL 5
#define REQUIRED_SAMPLES 2000

int xaxis_callibration_candidate[3];
int xzero;
int yaxis_callibration_candidate[3];
int yzero;
int samples;
float rounding_bufferx;
float rounding_buffery;
int middle_button;

uint8_t buttons;

void pointing_device_driver_init(){
	trackpoint_init();
	int x_rx=100;
	int y_rx=100;
	while (x_rx>5||y_rx>5){
		trackpoint_read_y(&y_rx);
		trackpoint_read_x(&x_rx);
	}
}

report_mouse_t pointing_device_driver_get_report(report_mouse_t mouse_report){
	int x_rx;
	int y_rx;
	
	trackpoint_read_y(&y_rx);
	trackpoint_read_x(&x_rx);

	mouse_report.buttons=buttons;

	if (y_rx>DEADZONE/2 || y_rx<-DEADZONE/2 || x_rx>DEADZONE/2 || x_rx<-DEADZONE/2){
		if ((mouse_report.buttons&0x04)==4){
			//float accel=(fabs(x_rx)+fabs(y_rx));
			rounding_bufferx += (0.0002*x_rx);
			rounding_buffery += (0.0002*y_rx);

			mouse_report.h = trunc(rounding_bufferx);
			mouse_report.v = trunc(rounding_buffery);

			rounding_bufferx = rounding_bufferx-trunc(rounding_bufferx);
			rounding_buffery = rounding_buffery-trunc(rounding_buffery);
			middle_button=-1;
			mouse_report.buttons=mouse_report.buttons&0xFB;
		}else{
			float accel=(pow(pow(x_rx,2)+pow(y_rx,2),.5));
			rounding_bufferx += 0.00005*x_rx*accel;
			rounding_buffery += -0.00005*y_rx*accel;

			mouse_report.x = trunc(rounding_bufferx);
			mouse_report.y = trunc(rounding_buffery);

			rounding_bufferx = rounding_bufferx-trunc(rounding_bufferx);
			rounding_buffery = rounding_buffery-trunc(rounding_buffery);
		}
	}
	else{
		if ((mouse_report.buttons&0x04)==4){
			mouse_report.buttons=mouse_report.buttons&0xFB;
			if (middle_button>=0){
				middle_button=1;
			}
		}
		else if (middle_button!=0){
			if(middle_button==1){
				mouse_report.buttons=mouse_report.buttons|0x04;
			}
			middle_button=0;
		}
	}

	return mouse_report;
}

void trackpoint_init(void){
	xaxis_callibration_candidate[0]=0;
	xaxis_callibration_candidate[1]=-1;
	xaxis_callibration_candidate[2]=0;

	yaxis_callibration_candidate[0]=0;
	yaxis_callibration_candidate[1]=-1;
	yaxis_callibration_candidate[2]=0;

	xzero=-1;
	yzero=-1;

	samples=0;

	rounding_bufferx=0;
	rounding_buffery=0;

	middle_button=0;
	adc_spi_init();
}

void trackpoint_read_x(int *out){
	int precalibration_out;
	adc_spi_read(&precalibration_out,1);
	int calibration=trackpoint_calibration(precalibration_out,xaxis_callibration_candidate);
	if (calibration>0){
		xzero=calibration;
	}
	*out=precalibration_out-xzero;
}

void trackpoint_read_y(int *out){
	int precalibration_out;
	adc_spi_read(&precalibration_out,2);
	int calibration=trackpoint_calibration(precalibration_out,yaxis_callibration_candidate);
	if (calibration>0){
		yzero=calibration;
	}
	*out=precalibration_out-yzero;
	
}

int trackpoint_calibration(int precalibration_out,int *callibration_candidate){
	int out=-1;
	int cal_test_positive=precalibration_out-callibration_candidate[1]-callibration_candidate[2];
	int cal_test_negative=precalibration_out-callibration_candidate[1]+callibration_candidate[0];
	if (cal_test_positive<=ZONECAL-(callibration_candidate[0]+callibration_candidate[2])){
		if(-cal_test_negative<=ZONECAL-(callibration_candidate[0]+callibration_candidate[2])){
			samples++;
			if (cal_test_positive>0){
				callibration_candidate[2]+=cal_test_positive;
			}
			else if (cal_test_negative<0){
				callibration_candidate[0]-=cal_test_negative;
			}
			if (samples>=REQUIRED_SAMPLES*2){
				out=callibration_candidate[1]+callibration_candidate[2]-callibration_candidate[0];
				if (samples>REQUIRED_SAMPLES*2){
					samples=0;
				}
			}
			return out;
		}
	}
	callibration_candidate[0]=0;
	callibration_candidate[1]=precalibration_out;
	callibration_candidate[2]=0;
	samples=0;
	return out;
}


bool process_record_kb(uint16_t keycode, keyrecord_t *record) {
    switch(keycode) {
        case KC_BTN1:
            if (record->event.pressed) {
				buttons=buttons|0x01;
            }
			else if (IS_RELEASED(record->event)) {
				buttons=buttons&0xFE;
            }
            return false;
        case KC_BTN2:
            if (record->event.pressed) {
				buttons=buttons|0x02;
            }
			else if (IS_RELEASED(record->event)) {
				buttons=buttons&0xFD;
            }
            return false;
        case KC_BTN3:
            if (record->event.pressed) {
				buttons=buttons|0x04;
            }
			else if (IS_RELEASED(record->event)) {
				buttons=buttons&0xFB;
            }
            return false;
    }
    return process_record_user(keycode, record);
};
