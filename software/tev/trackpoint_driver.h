/*enum keyboard_keycodes {
    KC_BTN= SAFE_RANGE,
    C_BTN2,
    KC_BTN3,
    NEW_SAFE_RANGE  // Important!
};*/

void trackpoint_init(void);
void trackpoint_read_x(int *);
void trackpoint_read_y(int *);
int trackpoint_calibration(int ,int *);
void pointing_device_driver_init(void);
