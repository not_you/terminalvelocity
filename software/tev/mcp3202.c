#include "spi_master.h"

void adc_spi_init(void){
	spi_init();
	setPinOutput(TRACKPOINT_CS_PIN);
	writePinHigh(TRACKPOINT_CS_PIN);
	writePinLow(TRACKPOINT_CS_PIN);
	writePinHigh(TRACKPOINT_CS_PIN);
}


void adc_spi_start(void) {                                                                                                                          
    spi_start(TRACKPOINT_CS_PIN, false, TRACKPOINT_SPI_MODE, TRACKPOINT_SPI_DIVISOR);                                                                                  
}

void adc_spi_stop(void){
	spi_stop();
}

void adc_spi_read(int *out,int axis){
	uint8_t write[3]={0x01,0xA0,0x00};
	if (axis==1){
		write[1]=0xA0;
	}
	else if (axis==2){
		write[1]=0xC0;
	}
	uint8_t read[3];
	adc_spi_start();
	writePinLow(TRACKPOINT_CS_PIN);
	spiExchange(&SPI_DRIVER,3,write,read);
	writePinHigh(TRACKPOINT_CS_PIN);
	*out=(read[1]&0x0F)*256+read[2];
	adc_spi_stop();
}
