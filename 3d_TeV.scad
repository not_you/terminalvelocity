$fn=50;
cleanup=0.01;

file="milling/case.dxf";

//use 8mm screws for trackpoint and lighting bar M2    bought

//10mm screws for plate M2.5	bought
plate=1.6;

//use 6mm screws for main pcb M2
//main pcb thickness is 1.2mm
main_cutout=4.6;

switch_cutout=2.1;

//use 2mm screws for usb pcb M2
//usb pcb thickness is .8mm
usb_cutout=2.3;
spacers=.6;
m25=2;
m2=1.6;
usb=7;
feet=.3;
text=.1;
height=10;
underglow=.8;
pins=.5;

debugging=1;




difference(){
	linear_extrude(height)
	import(file,layer="outside",$fn=200);
	
	translate([0,0,height-plate])
	linear_extrude(plate+cleanup)
	import(file,layer="inside",$fn=200);

	translate([0,0,height-plate-main_cutout])
	linear_extrude(main_cutout+cleanup)
	import(file,layer="plate_support");
	
	translate([0,0,height-plate-main_cutout-switch_cutout])
	linear_extrude(switch_cutout+cleanup)
	import(file,layer="switch_cutout");
	
	translate([0,0,height-plate-main_cutout-switch_cutout-pins])
	linear_extrude(pins+cleanup)
	import(file,layer="pins");

	translate([0,0,height-plate-main_cutout-underglow])
	linear_extrude(underglow+cleanup)
	import(file,layer="underglow");

	translate([0,0,-cleanup])
	linear_extrude(height*2+cleanup*2)
	import(file,layer="holes");

	translate([0,0,height-plate-main_cutout-usb_cutout])
	linear_extrude(usb_cutout+cleanup)
	import(file,layer="top_cutouts");
	
	if (debugging==1){
		translate([0,0,-cleanup])
		linear_extrude(height+cleanup*2)
		import(file,layer="debug_pocket");
	}

	translate([0,0,height-plate-spacers])
	linear_extrude(spacers+cleanup)
	import(file,layer="spacer_cutout");

	translate([0,0,-cleanup])
	linear_extrude(m25+cleanup)
	import(file,layer="M2.5_nuts");

	translate([0,0,-cleanup])
	linear_extrude(m2+cleanup)
	import(file,layer="M2_nuts");

	translate([0,0,-cleanup])
	linear_extrude(usb+cleanup)
	import("milling/feet_placement.dxf",layer="usb");

	translate([0,0,-cleanup])
	linear_extrude(feet+cleanup)
	import("milling/feet_placement.dxf",layer="feet");

	translate([0,0,-cleanup])
	linear_extrude(text+cleanup)
	import("milling/feet_placement.dxf",layer="text");
}

/*translate([0,0,height-plate-main_cutout])
color([0,.7,0])
import("PCBs/pcb/pcb.stl");*/
