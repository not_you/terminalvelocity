EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H5
U 1 1 6172B3D1
P 3200 6050
F 0 "H5" H 3300 6096 50  0000 L CNN
F 1 "MountingHole" H 3300 6005 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 3200 6050 50  0001 C CNN
F 3 "~" H 3200 6050 50  0001 C CNN
	1    3200 6050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 6172BAD0
P 2150 7300
F 0 "H1" H 2250 7346 50  0000 L CNN
F 1 "MountingHole" H 2250 7255 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 2150 7300 50  0001 C CNN
F 3 "~" H 2150 7300 50  0001 C CNN
	1    2150 7300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 6172BAD6
P 3700 7500
F 0 "H4" H 3800 7546 50  0000 L CNN
F 1 "MountingHole" H 3800 7455 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 3700 7500 50  0001 C CNN
F 3 "~" H 3700 7500 50  0001 C CNN
	1    3700 7500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H7
U 1 1 6172C180
P 2600 7450
F 0 "H7" H 2700 7496 50  0000 L CNN
F 1 "MountingHole" H 2700 7405 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 2600 7450 50  0001 C CNN
F 3 "~" H 2600 7450 50  0001 C CNN
	1    2600 7450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H13
U 1 1 6172C186
P 1500 6700
F 0 "H13" H 1600 6746 50  0000 L CNN
F 1 "MountingHole" H 1600 6655 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 1500 6700 50  0001 C CNN
F 3 "~" H 1500 6700 50  0001 C CNN
	1    1500 6700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H11
U 1 1 6172D5BE
P 5400 6700
F 0 "H11" H 5500 6746 50  0000 L CNN
F 1 "MountingHole" H 5500 6655 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 5400 6700 50  0001 C CNN
F 3 "~" H 5400 6700 50  0001 C CNN
	1    5400 6700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H17
U 1 1 6172D5C4
P 4350 7350
F 0 "H17" H 4450 7396 50  0000 L CNN
F 1 "MountingHole" H 4450 7305 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 4350 7350 50  0001 C CNN
F 3 "~" H 4350 7350 50  0001 C CNN
	1    4350 7350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H16
U 1 1 6172D5D0
P 3200 5850
F 0 "H16" H 3300 5896 50  0000 L CNN
F 1 "MountingHole" H 3300 5805 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 3200 5850 50  0001 C CNN
F 3 "~" H 3200 5850 50  0001 C CNN
	1    3200 5850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H25
U 1 1 6172EFA4
P 4200 6300
F 0 "H25" H 4300 6346 50  0000 L CNN
F 1 "MountingHole" H 4300 6255 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 4200 6300 50  0001 C CNN
F 3 "~" H 4200 6300 50  0001 C CNN
	1    4200 6300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H18
U 1 1 6172EFAA
P 4800 6500
F 0 "H18" H 4900 6546 50  0000 L CNN
F 1 "MountingHole" H 4900 6455 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 4800 6500 50  0001 C CNN
F 3 "~" H 4800 6500 50  0001 C CNN
	1    4800 6500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H27
U 1 1 6172EFBC
P 5500 7300
F 0 "H27" H 5600 7346 50  0000 L CNN
F 1 "MountingHole" H 5600 7255 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 5500 7300 50  0001 C CNN
F 3 "~" H 5500 7300 50  0001 C CNN
	1    5500 7300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H26
U 1 1 6172EFC8
P 3700 6750
F 0 "H26" H 3800 6796 50  0000 L CNN
F 1 "MountingHole" H 3800 6705 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 3700 6750 50  0001 C CNN
F 3 "~" H 3700 6750 50  0001 C CNN
	1    3700 6750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H23
U 1 1 6172EFCE
P 2700 6750
F 0 "H23" H 2800 6796 50  0000 L CNN
F 1 "MountingHole" H 2800 6705 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 2700 6750 50  0001 C CNN
F 3 "~" H 2700 6750 50  0001 C CNN
	1    2700 6750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H29
U 1 1 6172EFD4
P 1350 7300
F 0 "H29" H 1450 7346 50  0000 L CNN
F 1 "MountingHole" H 1450 7255 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 1350 7300 50  0001 C CNN
F 3 "~" H 1350 7300 50  0001 C CNN
	1    1350 7300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H22
U 1 1 6172EFDA
P 2000 6600
F 0 "H22" H 2100 6646 50  0000 L CNN
F 1 "MountingHole" H 2100 6555 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 2000 6600 50  0001 C CNN
F 3 "~" H 2000 6600 50  0001 C CNN
	1    2000 6600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H28
U 1 1 6172EFE0
P 2450 6350
F 0 "H28" H 2550 6396 50  0000 L CNN
F 1 "MountingHole" H 2550 6305 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_DIN965" H 2450 6350 50  0001 C CNN
F 3 "~" H 2450 6350 50  0001 C CNN
	1    2450 6350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 618C8170
P 4600 4300
F 0 "J1" H 4628 4276 50  0000 L CNN
F 1 "Conn_01x04_Female" H 4628 4185 50  0000 L CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x04_P1.27mm_Vertical_M50-3130445_UpsideDown" H 4600 4300 50  0001 C CNN
F 3 "~" H 4600 4300 50  0001 C CNN
	1    4600 4300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 618CC0ED
P 4500 4500
F 0 "#PWR0101" H 4500 4250 50  0001 C CNN
F 1 "GND" H 4505 4327 50  0000 C CNN
F 2 "" H 4500 4500 50  0001 C CNN
F 3 "" H 4500 4500 50  0001 C CNN
	1    4500 4500
	1    0    0    -1  
$EndComp
Text GLabel 4600 4500 3    50   Input ~ 0
data
Text GLabel 4700 4500 3    50   Input ~ 0
clock
$Comp
L power:+5V #PWR0102
U 1 1 618CCE89
P 4800 4500
F 0 "#PWR0102" H 4800 4350 50  0001 C CNN
F 1 "+5V" H 4815 4673 50  0000 C CNN
F 2 "" H 4800 4500 50  0001 C CNN
F 3 "" H 4800 4500 50  0001 C CNN
	1    4800 4500
	-1   0    0    1   
$EndComp
$Comp
L Device:C C1
U 1 1 618CD492
P 5600 4200
F 0 "C1" H 5715 4246 50  0000 L CNN
F 1 "2.2Uf" H 5715 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5638 4050 50  0001 C CNN
F 3 "~" H 5600 4200 50  0001 C CNN
	1    5600 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 618CD943
P 5600 4500
F 0 "R1" H 5670 4546 50  0000 L CNN
F 1 "100K" H 5670 4455 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5530 4500 50  0001 C CNN
F 3 "~" H 5600 4500 50  0001 C CNN
	1    5600 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 618CDF7E
P 5600 4650
F 0 "#PWR0103" H 5600 4400 50  0001 C CNN
F 1 "GND" H 5605 4477 50  0000 C CNN
F 2 "" H 5600 4650 50  0001 C CNN
F 3 "" H 5600 4650 50  0001 C CNN
	1    5600 4650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 618CE32F
P 5600 4050
F 0 "#PWR0104" H 5600 3900 50  0001 C CNN
F 1 "+5V" H 5615 4223 50  0000 C CNN
F 2 "" H 5600 4050 50  0001 C CNN
F 3 "" H 5600 4050 50  0001 C CNN
	1    5600 4050
	1    0    0    -1  
$EndComp
Text GLabel 5600 4350 0    50   Input ~ 0
reset
$Comp
L trackpoints:tp_xX80 T1
U 1 1 618EA70F
P 3700 4550
F 0 "T1" H 3150 5200 50  0000 C CNN
F 1 "tp_xX80" H 3250 5100 50  0000 C CNN
F 2 "trackpoints:xX80_PCB" H 3350 5150 50  0001 C CNN
F 3 "" H 3350 5150 50  0001 C CNN
	1    3700 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 618EB656
P 3700 4000
F 0 "#PWR0105" H 3700 3750 50  0001 C CNN
F 1 "GND" H 3705 3827 50  0000 C CNN
F 2 "" H 3700 4000 50  0001 C CNN
F 3 "" H 3700 4000 50  0001 C CNN
	1    3700 4000
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 618EC63C
P 3700 5100
F 0 "#PWR0106" H 3700 4950 50  0001 C CNN
F 1 "+5V" H 3715 5273 50  0000 C CNN
F 2 "" H 3700 5100 50  0001 C CNN
F 3 "" H 3700 5100 50  0001 C CNN
	1    3700 5100
	-1   0    0    1   
$EndComp
Text GLabel 4050 4350 2    50   Input ~ 0
clock
Text GLabel 4050 4450 2    50   Input ~ 0
data
Text GLabel 4050 4550 2    50   Input ~ 0
reset
$Comp
L Connector:Conn_01x05_Female J2
U 1 1 618F478A
P 3050 4550
F 0 "J2" H 3078 4576 50  0000 L CNN
F 1 "Conn_01x05_Female" H 3078 4485 50  0000 L CNN
F 2 "trackpoints:xX80_flexpoint" H 3050 4550 50  0001 C CNN
F 3 "~" H 3050 4550 50  0001 C CNN
	1    3050 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 4350 2850 4350
Wire Wire Line
	2850 4450 3300 4450
Wire Wire Line
	3300 4550 2850 4550
Wire Wire Line
	2850 4650 3300 4650
Wire Wire Line
	3300 4750 2850 4750
$EndSCHEMATC
